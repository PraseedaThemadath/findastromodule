# FindAstro Module
Steps to integrate FindAstro module in a Project

A) Integrating as Sub-Module

1. Add FindAstro Module as a git submodule in project
	git submodule add <url>/findastromodule.git

2. file > project structure
3. click ‘+’ on left top
4. Select “Import gradle project”
5. Select the sub-module folder
6. Give the sub-module project name without conflicting main project name
7. Sync and clean build

8. Add project in app’s build gradle
    implementation project(':<subModulename>)

9. Sync and clean

B) Steps to use FindAstro in Project

1. Add FindAstroFragment class wherever the FinAstro flow need to be implemented 
2. Implement Interface “IUserAuthenticate” in the attaching activity else a ClassCastException will be thrown
3. Request to initiate google sign in will be received on “initiateUserAuthentication” method of “IUserAuthenticate” implemented activity.
4. After successful google sign-in , call “ onSignInSuccess” method of “FindAstroFragment” to continue user registration/login
