package com.clickastro.module.findastro.utilities

import com.clickastro.module.findastro.model.*
import com.clickastro.module.findastro.model.RegisterModel.OtpSuccessRegisterModel
import com.clickastro.module.findastro.model.loginModel.OtpSuccessLoginModel
import com.clickastro.module.findastro.model.requestotp.OtpModel
import retrofit2.Call
import retrofit2.http.*

interface FindastroApiCalls {
    @POST("user/otp")
    fun getrequestsignupotp(@Query("phone") phone: String): Call<OtpModel>

    @POST("user/otp/{id}")
    fun getrequestsignupresendotp(@Path("id") id: String?, @Query("phone") phone: String): Call<OtpModel>

    @POST("user/otp/register/{id}")
    fun enterregisterotp(@Path("id") id: String?, @Query("phone") phone: String,@Query("otp") otp: String,@Query("is_trai") trai:String,@Query("is_whatsapp") whatsapp:String,@Query("data_dom") data_dom:String): Call<OtpSuccessRegisterModel>

    @POST("user/otp/login/{id}")
    fun enterloginotp(@Path("id") id: String?, @Query("phone") phone: String,@Query("otp") otp: String,@Query("is_trai") trai:String,@Query("is_whatsapp") whatsapp:String,@Query("data_dom") data_dom:String): Call<OtpSuccessLoginModel>

//    @Headers("Content-Type: application/json")
    @POST("profile/user/language")
    fun languageSelectioncall(@Header("Authorization") auth:String, @Header("token") token:String, @Body langmodel: LanguageRequestModel) : Call<LanguageResponseModel>

    @POST("profile/user/edit")
    fun adduserapicall(@Header("Authorization") auth:String,@Header("token") token:String,@Body langmodel: AddUserRequestModel) : Call<AddUserResponseModel>

    @POST("user/upload-access")
    fun imgupload(@Header("Authorization") auth:String,@Header("token") token:String,@Query("fileName") filename: String,@Query("fileType") filetype: String): Call<ImageAccessResponseModel>

    @PUT("{path}")
    fun uploadimgs3(@Path("path") path: String?, @Body file: String, @Body stringdummy: String):Call<String>
}
