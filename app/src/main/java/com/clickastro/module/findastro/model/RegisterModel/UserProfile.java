package com.clickastro.module.findastro.model.RegisterModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {

@SerializedName("name")
@Expose
private Object name;
@SerializedName("email")
@Expose
private Object email;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("profileUrl")
@Expose
private Object profileUrl;

public Object getName() {
return name;
}

public void setName(Object name) {
this.name = name;
}

public Object getEmail() {
return email;
}

public void setEmail(Object email) {
this.email = email;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public Object getProfileUrl() {
return profileUrl;
}

public void setProfileUrl(Object profileUrl) {
this.profileUrl = profileUrl;
}

}