package com.clickastro.module.findastro.model.loginModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {

@SerializedName("name")
@Expose
private String name;
@SerializedName("email")
@Expose
private String email;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("profileUrl")
@Expose
private String profileUrl;
@SerializedName("balance")
@Expose
private String balance;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getProfileUrl() {
return profileUrl;
}

public void setProfileUrl(String profileUrl) {
this.profileUrl = profileUrl;
}

public String getBalance() {
return balance;
}

public void setBalance(String balance) {
this.balance = balance;
}

}