package com.clickastro.module.findastro.model.requestotp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("id")
@Expose
private Integer id;
@SerializedName("expiry")
@Expose
private String expiry;
@SerializedName("to")
@Expose
private String to;
@SerializedName("via")
@Expose
private String via;
@SerializedName("otp_length")
@Expose
private Integer otpLength;
@SerializedName("resend_after")
@Expose
private Integer resendAfter;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getExpiry() {
return expiry;
}

public void setExpiry(String expiry) {
this.expiry = expiry;
}

public String getTo() {
return to;
}

public void setTo(String to) {
this.to = to;
}

public String getVia() {
return via;
}

public void setVia(String via) {
this.via = via;
}

public Integer getOtpLength() {
return otpLength;
}

public void setOtpLength(Integer otpLength) {
this.otpLength = otpLength;
}

public Integer getResendAfter() {
return resendAfter;
}

public void setResendAfter(Integer resendAfter) {
this.resendAfter = resendAfter;
}

}