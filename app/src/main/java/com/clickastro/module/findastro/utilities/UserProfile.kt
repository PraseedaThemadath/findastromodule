package com.clickastro.module.findastro.utilities

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class UserProfile() : Parcelable {
    private var userId: Long = 0
    private var userName: String? = null
    private var userDob: String? = null
    private var userTob: String? = null
    private var userLang: String? = null
    private var userGender = FindAstroConstants.BLANK_STRING
    private var userPob: String? = null
    private var userPlaceJson: String? = null
    private var userDailyHoroscope: String? = null
    private var userHoroscope: String? = null
    private var HoroscopeStyle: String? = null

    val PICK_PROFILE = 1005
    var OPTED_PROFILE: Long = 0

    constructor(parcel: Parcel) : this() {
        userId = parcel.readLong()
        userName = parcel.readString()
        userDob = parcel.readString()
        userTob = parcel.readString()
        userLang = parcel.readString()
        userGender = parcel.readString()
        userPob = parcel.readString()
        userPlaceJson = parcel.readString()
        userDailyHoroscope = parcel.readString()
        userHoroscope = parcel.readString()
        HoroscopeStyle = parcel.readString()
        OPTED_PROFILE = parcel.readLong()
    }

    fun getOptedProfile(): Long {
        return OPTED_PROFILE
    }

    fun setOptedProfile(optedProfile: Long) {
        OPTED_PROFILE = optedProfile
    }


    fun getUserId(): Long {
        return userId
    }

    fun setUserId(userId: Long) {
        this.userId = userId
    }

    fun getUserName(): String? {
        return userName
    }

    fun setUserName(userName: String?) {
        this.userName = userName
    }

    fun getUserDob(): String? {
        return userDob
    }

    fun setUserDob(userDob: String?) {
        this.userDob = userDob
    }

    fun getUserTob(): String? {
        return userTob
    }

    fun setUserTob(userTob: String?) {
        this.userTob = userTob
    }

    fun getUserLang(): String? {
        return userLang
    }

    fun setUserLang(userLang: String?) {
        this.userLang = userLang
    }

    fun getUserGender(): String? {
        return userGender
    }

    fun setUserGender(userGender: String) {
        this.userGender = userGender
    }

    fun getUserPob(): String? {
        return userPob
    }

    fun setUserPob(userPob: String?) {
        this.userPob = userPob
    }

    fun getUserPlaceJson(): String? {
        return userPlaceJson
    }

    @Throws(JSONException::class)
    fun getUserPlaceJsonObject(): JSONObject? {
        return if (userPlaceJson != null) {
            JSONObject(userPlaceJson)
        } else {
            JSONObject()
        }
    }

    fun setUserPlaceJson(userPlaceJson: String?) {
        this.userPlaceJson = userPlaceJson
    }


    fun getUserDailyHoroscope(): String? {
        return userDailyHoroscope
    }

    fun setUserDailyHoroscope(userDailyHoroscope: String?) {
        this.userDailyHoroscope = userDailyHoroscope
    }

    fun getUserHoroscope(): String? {
        return userHoroscope
    }

    fun setUserHoroscope(userHoroscope: String?) {
        this.userHoroscope = userHoroscope
    }

    fun setHoroscopeStyle(horoscope_style: String?) {
        HoroscopeStyle = horoscope_style
    }

    fun getHoroscopeStyle(): String? {
        return if (HoroscopeStyle == null || HoroscopeStyle == FindAstroConstants.BLANK_STRING) {
            "0"
        } else {
            HoroscopeStyle
        }
    }

    fun getUserDobDateTime(): Calendar? {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH)
        try {
            cal.time = sdf.parse(getUserDob() + " " + getUserTob()) // all done
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return cal
    }

    fun resetNotifications(context: Context) {
        val spf = context.getSharedPreferences("NOTIF_PREF", Context.MODE_PRIVATE)
        spf.edit().putBoolean("ActiveNotification", false).apply()
    }

    fun enableNotifications(context: Context) {
        val spf = context.getSharedPreferences("NOTIF_PREF", Context.MODE_PRIVATE)
        spf.edit().putBoolean("ActiveNotification", true).apply()
    }

    fun isNotificationsActive(context: Context): Boolean {
        val spf = context.getSharedPreferences("NOTIF_PREF", Context.MODE_PRIVATE)
        return spf.getBoolean("ActiveNotification", false)
    }

    fun setEnableNotifications(context: Context, checked: Boolean) {
        val spf = context.getSharedPreferences("NOTIF_PREF", Context.MODE_PRIVATE)
        spf.edit().putBoolean("showNotifications", checked).apply()
    }

    fun isNotificationsEnabled(context: Context): Boolean {
        val spf = context.getSharedPreferences("NOTIF_PREF", Context.MODE_PRIVATE)
        return spf.getBoolean("showNotifications", true)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(userId)
        parcel.writeString(userName)
        parcel.writeString(userDob)
        parcel.writeString(userTob)
        parcel.writeString(userLang)
        parcel.writeString(userGender)
        parcel.writeString(userPob)
        parcel.writeString(userPlaceJson)
        parcel.writeString(userDailyHoroscope)
        parcel.writeString(userHoroscope)
        parcel.writeString(HoroscopeStyle)
        parcel.writeLong(OPTED_PROFILE)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserProfile> {
        override fun createFromParcel(parcel: Parcel): UserProfile {
            return UserProfile(parcel)
        }

        override fun newArray(size: Int): Array<UserProfile?> {
            return arrayOfNulls(size)
        }
    }


}