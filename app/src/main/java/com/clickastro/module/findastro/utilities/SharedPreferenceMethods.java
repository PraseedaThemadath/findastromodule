package com.clickastro.module.findastro.utilities;

/*Created by Santhi J Krishnan on 03-04-2019
 *
 * Getter and setter methods to store and retrieve key-value pairs in android shared preference*/


import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.clickastro.module.findastro.utilities.FindAstroConstants.BLANK_STRING;
import static com.clickastro.module.findastro.utilities.FindAstroConstants.SHARED_PREFERENCE;

public class SharedPreferenceMethods {

    /*store string value into shared preference**/
    public static void setToSharedPreference(Context context, String key, String value) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.putString(key, value);
        preferenceEditor.apply();
    }

    /*remove string value from shared preference**/
    public static void removeSharedPreference(Context context, String key) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.remove(key);
        preferenceEditor.apply();
    }

    /*retrieve string value from shared preference**/
    public static String getFromSharedPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        return prefs.getString(key, BLANK_STRING);
    }

    /*store boolean value into shared preference**/
    public static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.putBoolean(key, value);
        preferenceEditor.apply();
    }

    /*remove boolean value from shared preference**/
    public static void removeBoolean(Context context, String key) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.remove(key);
        preferenceEditor.apply();
    }

    /*retrieve boolean value from shared preference**/
    public static Boolean getBoolean(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        return prefs.getBoolean(key, false);
    }

    /*retrieve boolean value from shared preference**/
    public static Boolean isFirstLaunch(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        return prefs.getBoolean(key, true);
    }

    /*store integer value into shared preference**/
    public static void setIntToSharedPreference(Context context, String key, int value) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.putInt(key, value);
        preferenceEditor.apply();
    }

    /*remove integer value from shared preference**/
    public static void removeIntegerSharedPreference(Context context, String key) {
        SharedPreferences.Editor preferenceEditor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        preferenceEditor.remove(key);
        preferenceEditor.apply();
    }

    /*retrieve integer value from shared preference**/
    public static Integer getIntFromSharedPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        return prefs.getInt(key, 0);
    }
}
