package com.clickastro.module.findastro

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.webkit.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.clickastro.module.findastro.utilities.*
import com.clickastro.module.findastro.utilities.FindAstroConstants.*
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


private var mActivity: AppCompatActivity? = null
private lateinit var mContext: Context
private val instance: WebViewFindAstro? = null
private var deepLinkAction = BLANK_STRING
private var mTimer = CustomTimer(Handler())
private var deepLinkData = BLANK_STRING
private var token: String = ""
private var email: String = ""
private lateinit var webview: WebView
lateinit var urldata: String
const val REQUEST_CODE_LOLIPOP = 1
private const val RESULT_CODE_ICE_CREAM = 2
lateinit var nointernetimg: ImageView
lateinit var refreshbtn: Button
lateinit var nointerneterrorlayout: LinearLayout
private var mFilePathCallback: ValueCallback<Array<Uri>>? = null
private var mCameraPhotoPath: String? = null
private var mUploadMessage: ValueCallback<Uri>? = null
//private lateinit var mContext: Context
lateinit var urlphone: String
private var mWebviewPop: WebView? = null
private var dialog: AlertDialog? = null
private var builder: AlertDialog? = null
private  var mUser: UserProfile? = null


private const val uanormal = "Mozilla/5.0 (Linux; U; Android 2.3.3; en-gb; " +
        "Nexus S Build/GRI20) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"

class WebViewFindAstro : Fragment() {
    companion object {
        private var instance: WebViewFindAstro? = null
        fun getInstance(action: String?, data: String?): WebViewFindAstro? = WebViewFindAstro().apply {
            arguments = bundleOf(deepLinkData to data, deepLinkAction to action)
            return instance
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.webview_findastro_layout, container, false)
    }

    override fun onStart() {
        super.onStart()
        webview.reload()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initvariables(view)
        mUser = requireArguments().getParcelable("user")
        token = arguments?.getString("token").toString()
        email = arguments?.getString("email").toString()
        SharedPreferenceMethods.setBoolean(activity, "loadwebview", true)
        SharedPreferenceMethods.setToSharedPreference(activity, "email", email)
        SharedPreferenceMethods.setToSharedPreference(activity, "token", token)
        SharedPreferenceMethods.setBoolean(activity, "isfirstlaunch", false)

        val handler: Handler = @SuppressLint("HandlerLeak")
        object : Handler() {
            override fun handleMessage(message: Message) {
                when (message.what) {
                    1 -> {
                        webViewGoBack()
                    }
                }
            }
        }

        val webSettings: WebSettings = webview.settings
        val cachePath = requireActivity().applicationContext.cacheDir.absolutePath
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.builtInZoomControls = true
        webSettings.displayZoomControls = false
        webSettings.setSupportZoom(true)
        webSettings.setAppCacheMaxSize(1024 * 1024 * 8);
        webSettings.setAppCachePath(cachePath)
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.userAgentString = uanormal + " fa-wv"
        webSettings.setSupportMultipleWindows(true)
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true)

        webview.setWebChromeClient(object : WebChromeClient() {
            override fun onShowFileChooser(
                    webView: WebView?, filePathCallback: ValueCallback<Array<Uri>>?,
                    fileChooserParams: FileChooserParams?): Boolean {
                if (mFilePathCallback != null) {
                    mFilePathCallback!!.onReceiveValue(null)
                }
                mFilePathCallback = filePathCallback
                var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent!!.resolveActivity(requireActivity().packageManager) != null) {
                    // Create the File where the photo should go
                    var photoFile: File? = null
                    try {
                        photoFile = createImageFile()
                        takePictureIntent!!.putExtra("PhotoPath", mCameraPhotoPath)
                    } catch (ex: IOException) {
                        // Error occurred while creating the File
                        Log.e("TAG", "Unable to create Image File", ex)
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        mCameraPhotoPath = "file:" + photoFile.absolutePath
                        takePictureIntent!!.putExtra(
                                MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile))
                    } else {
                        takePictureIntent = null
                    }
                }
                val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                contentSelectionIntent.type = "image/*"
                val intentArray: Array<Intent?>
                intentArray = takePictureIntent?.let { arrayOf(it) } ?: arrayOfNulls(0)
                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                startActivityForResult(chooserIntent, REQUEST_CODE_LOLIPOP)
                return true
            }

            override fun onCreateWindow(view: WebView?, userGesture: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {
                mWebviewPop = WebView(requireContext())
                mWebviewPop = WebView(requireContext())
                mWebviewPop!!.webChromeClient = WebChromeClient()
                mWebviewPop!!.settings.javaScriptEnabled = true
                mWebviewPop!!.settings.savePassword = true
                mWebviewPop!!.settings.saveFormData = true
                mWebviewPop!!.settings.userAgentString = uanormal
                mWebviewPop!!.isVerticalScrollBarEnabled = false
                mWebviewPop!!.isHorizontalScrollBarEnabled = false

                mWebviewPop!!.webViewClient = object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                        if (url.equals("https://play.google.com/store/apps/details?id=com.clickastro.findastro&referrer=utm_source%3DFindastro_web%26utm_medium%3Dapplink")) {
                            try {
                                val i = Intent(Intent.ACTION_VIEW)
                                val url = "https://play.google.com/store/apps/details?id=com.clickastro.findastro"
                                i.data = Uri.parse(url)
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                mActivity!!.startActivity(i)
                            } catch (e: ActivityNotFoundException) {
                                val i = Intent(Intent.ACTION_VIEW)
                                val url = "market://details?id=com.clickastro.findastro"
                                i.data = Uri.parse(url)
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                mActivity!!.startActivity(i)
                            }
                        } else {
                            webview.loadUrl(url)
                        }
                        return true
                    }

                    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                    }

                    override fun onPageFinished(view: WebView, url: String) {
                        // Page loading finished
                    }
                }
//
                val cookieManager = CookieManager.getInstance()
                cookieManager.setAcceptCookie(true)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true)
                }
                val transport: WebView.WebViewTransport = resultMsg!!.obj as WebView.WebViewTransport
                transport.webView = mWebviewPop
                resultMsg.sendToTarget()
//                builder!!.show()
                return true;
            }
        })

        webview.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() === MotionEvent.ACTION_UP && webview.canGoBack()) {
                    handler.sendEmptyMessage(1)
                    return true
                }
                return false
            }
        })
        webview.addJavascriptInterface(WebAppInterface(mContext), JAVASCRIPT_IDENTIFIER)
        webSettings.defaultTextEncodingName = "utf-8"
        webview.webViewClient = MyWebViewClient(requireActivity())
        webview.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 2)
                } else {
                    ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 2)
                }
            }
            if ((ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) === PackageManager.PERMISSION_GRANTED)) {
                downloadDialog(url, userAgent, contentDisposition, mimetype)
            }
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webview.getSettings().setDatabasePath("/data/data/" + webview.getContext().getPackageName() + "/databases/")
        }
        if (FindAstroStaticMethods.isNetworkAvailable(requireActivity())) {
            webview.visibility = View.VISIBLE
            nointerneterrorlayout.visibility = View.GONE
            webview.loadUrl(FINDASTRO_BASE_URL)
        } else {
            webview.visibility = View.GONE
            Glide.with(this).load(R.drawable.nointernetgif).into(nointernetimg)
            nointerneterrorlayout.visibility = View.VISIBLE
        }

        refreshbtn.setOnClickListener {
            if (FindAstroStaticMethods.isNetworkAvailable(requireContext())) {
                webview.visibility = View.VISIBLE
                nointerneterrorlayout.visibility = View.GONE
                webview.loadUrl(FINDASTRO_BASE_URL)
            } else {
                webview.visibility = View.GONE
                Glide.with(this).load(R.drawable.nointernetgif).into(nointernetimg)
                nointerneterrorlayout.visibility = View.VISIBLE
            }
        }
        val appName = getString(R.string.app_name).replace(": ","_").replace(" ","_")
        var urlstring = FINDASTRO_BASE_URL + "app/login?phone=" + email + "&token=" + token +
                "&utm_source=ClickAstro_app&utm_medium=App_launch&utm_campaign=Android_app&utm_client=" +
                appName +"&utm_platform=android"
        webview.loadUrl(urlstring)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        webview.saveState(outState)
        super.onSaveInstanceState(outState!!)
    }

    class WebAppInterface  // Instantiate the interface and set the context
    internal constructor(var mContext: Context) {
        @android.webkit.JavascriptInterface
        fun call(method: String, payload: String): String {
            if (method.equals("pushUserPhone")) {
                //MoEngageEventTracker.setUniqueId(mContext, payload)
            } else if (method.equals("pushUserProfileDetails")) {
                var jsonObject: JSONObject? = mUser?.getUserPlaceJsonObject();
                val format = SimpleDateFormat("dd/MM/yyyy")
                try {
                    var date1 = jsonObject?.get("dateOfBirth").toString()
                    var date2 = date1.replace("\\", "")
                    var date = format.parse(date2.trim())
                    //MoEngageEventTracker.setTrackingUserAttributes(StaticMethods.getDefaultUser(mContext), jsonObject.get("email").toString(), mContext)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            } else if (method.equals("getAppVersion")) {
                return androidx.multidex.BuildConfig.VERSION_NAME
            } else if (method.equals("pushShareDetails")) {
                val intent = Intent(Intent.ACTION_SEND)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_SUBJECT, "FindAstro")
                intent.putExtra(Intent.EXTRA_TEXT, "Hey, checkout this astrologer " + urldata)
                mContext.startActivity(intent)
            }
            return ""
        }

        @android.webkit.JavascriptInterface
        fun call(method: String): String {
            return call(method, "")
        }
    }

    private fun webViewGoBack() {
        if (webview.canGoBack()) {
            webview.goBack();
        }
        if (urldata == FINDASTRO_BASE_URL) {
            activity?.onBackPressed();
        }
    }

    private fun initvariables(view: View) {
        webview = view.findViewById(R.id.webviewfindastro)
        nointernetimg = view.findViewById(R.id.nointernetimg)
        nointerneterrorlayout = view.findViewById(R.id.nointerneterrorlayout)
        refreshbtn = view.findViewById(R.id.refreshinternet)
    }

    fun downloadDialog(url: String?, userAgent: String?, contentDisposition: String?, mimetype: String?) {
        //getting filename from url.
        val filename = URLUtil.guessFileName(url, contentDisposition, mimetype)
        //alertdialog
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())

        //title of alertdialog
        builder.setTitle("Invoice Download")
        //message of alertdialog
        builder.setMessage("Do you want to download your invoice?")
        //if Yes button clicks.
        builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which -> //DownloadManager.Request created with url.
            val request = DownloadManager.Request(Uri.parse(url))
            //cookie
            val cookie = CookieManager.getInstance().getCookie(url)
            //Add cookie and User-Agent to request
            request.addRequestHeader("Cookie", cookie)
            request.addRequestHeader("User-Agent", userAgent)
            //file scanned by MediaScannar
            request.allowScanningByMediaScanner()
            //Download is visible and its progress, after completion too.
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            //DownloadManager created
            val downloadManager = activity?.getSystemService(AppCompatActivity.DOWNLOAD_SERVICE) as DownloadManager
            //Saving files in Download folder
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
            //download enqued
            downloadManager.enqueue(request)
        })
        builder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, which -> //cancel the dialog if Cancel clicks
            dialog.cancel()
            webview.goBack()
        })
        //alertdialog shows.
        builder.show()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(requireContext(),
                                    Manifest.permission.ACCESS_FINE_LOCATION) ===
                                    PackageManager.PERMISSION_GRANTED)) {
                        val intent = Intent(Intent.ACTION_CALL, Uri.parse(urlphone))
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        requireContext().startActivity(intent)
                        val appName = getString(R.string.app_name).replace(": ","_").replace(" ","_")
                        val urlstring = FINDASTRO_BASE_URL + "/app/login?phone=" + email + "&token=" + token +
                                "&utm_source=FindAstro_app&utm_medium=App_launch&utm_campaign=Android_app&utm_client=" +
                                appName +"&utm_platform=android"
                        webview.loadUrl(urlstring)
                    }
                }
                return
            }
        }
    }

    override fun onResume() {
        super.onResume()
        webview.reload()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        if (context is AppCompatActivity) {
            mActivity = context
        }
    }

    /*return webView***/
    fun getWebView(): WebView {
        return webview
    }
}

class MyWebViewClient internal constructor(private val activity: Activity) : WebViewClient() {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        val url: String = request?.url.toString();
        if (FindAstroStaticMethods.isNetworkAvailable(mContext)) {
            if (url.equals("https://play.google.com/store/apps/details?id=com.clickastro.findastro&referrer=utm_source%3DFindastro_web%26utm_medium%3Dapplink")) {
                try {
                    val i = Intent(Intent.ACTION_VIEW)
                    val url = "https://play.google.com/store/apps/details?id=com.clickastro.findastro"
                    i.data = Uri.parse(url)
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    mContext.startActivity(i)
                } catch (e: ActivityNotFoundException) {
                    val i = Intent(Intent.ACTION_VIEW)
                    val url = "market://details?id=com.clickastro.findastro"
                    i.data = Uri.parse(url)
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    mContext.startActivity(i)
                }
            } else {
                view?.loadUrl(url)
            }

            if (url.equals("mailto:support@findastro.com")) {
                val intent = Intent(Intent.ACTION_SENDTO)
                intent.data = Uri.parse("mailto:") // only email apps should handle this
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("support@findastro.com"))
                intent.putExtra(Intent.EXTRA_SUBJECT, "")
                mContext.startActivity(intent)
                webview.loadUrl(FINDASTRO_BASE_URL)
            }

            if (url != null) {
                urlphone = url
            }

            if (url.equals("tel:+918047248888")) {

                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                    Manifest.permission.CALL_PHONE)) {
                        ActivityCompat.requestPermissions(activity,
                                arrayOf(Manifest.permission.CALL_PHONE), 1)
                    } else {
                        ActivityCompat.requestPermissions(activity,
                                arrayOf(Manifest.permission.CALL_PHONE), 1)
                    }
                    webview.loadUrl(FINDASTRO_BASE_URL)
                } else {
                    var intent = Intent(Intent.ACTION_CALL, Uri.parse(url))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    mContext.startActivity(intent)
                    webview.loadUrl(FINDASTRO_BASE_URL)
                }
            }

            if (urldata.contains("whatsapp://send/?text=") || urldata.contains("https://api.whatsapp.com/send")) {
                try {
                    val packageManager: PackageManager = mContext.getPackageManager()
                    val i = Intent(Intent.ACTION_VIEW)
                    val url = "https://api.whatsapp.com/send?phone=" + "" + "&text=" + URLEncoder.encode("Hey checkout this app , https://play.google.com/store/apps/details?id=com.clickastro.findastro", "UTF-8")
                    i.setPackage("com.whatsapp")
                    i.data = Uri.parse(url)
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    if (i.resolveActivity(packageManager) != null) {
                        mContext.startActivity(i)
                    } else {
                    }
                    webview.loadUrl(FINDASTRO_BASE_URL)
                } catch (e: java.lang.Exception) {
                    Log.e("ERROR WHATSAPP", e.toString())
                }

            }
        } else {
            webview.visibility = View.GONE
            Glide.with(mContext).load(R.drawable.nointernetgif).into(nointernetimg)
            nointerneterrorlayout.visibility = View.VISIBLE
        }
        return true
    }

    override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
        if (FindAstroStaticMethods.isNetworkAvailable(mActivity!!)) {
            if (url.equals("mailto:support@findastro.com")) {

            } else {
                webView.loadUrl(url)
            }
        } else {
            webview.visibility = View.GONE
            Glide.with(mActivity!!).load(R.drawable.nointernetgif).into(nointernetimg)
            nointerneterrorlayout.visibility = View.VISIBLE
        }
        return true
    }

    override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
        //MoEngageEventTracker.errorPings(activity, " $error")
    }

    override fun doUpdateVisitedHistory(view: WebView?, url: String?, isReload: Boolean) {

        if (FindAstroStaticMethods.isNetworkAvailable(mActivity!!)) {
            if (url.equals("mailto:support@findastro.com")) {

            } else {
                super.doUpdateVisitedHistory(view, url, isReload)
                urldata = url.toString()
            }
        } else {
            webview.visibility = View.GONE
            Glide.with(mActivity!!).load(R.drawable.nointernetgif).into(nointernetimg)
            nointerneterrorlayout.visibility = View.VISIBLE
        }
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        urldata = view!!.url.toString()
        if (urldata == FINDASTRO_BASE_URL + "app/logout") {
            SharedPreferenceMethods.setBoolean(activity, "isRegistered", true)
            SharedPreferenceMethods.setBoolean(activity, "loadwebview", true)
            SharedPreferenceMethods.setToSharedPreference(activity, "token", "null")
            SharedPreferenceMethods.setBoolean(activity, "loadwebview", false)
            val fragmentTransaction = (activity as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayoutfindastro, LoginFragmentFindAstro())
            fragmentTransaction.commit()
        }

        if (url.equals("tel:+918047248888")) {

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
                val appName = activity.getString(R.string.app_name).replace(": ","_").replace(" ","_")
                var urlstring = FINDASTRO_BASE_URL + "app/login?phone=" + email + "&token=" + token +
                        "&utm_source=FindAstro_app&utm_medium=App_launch&utm_campaign=Android_app&utm_client=" +
                        appName +"&utm_platform=android"
                webview.loadUrl(urlstring)
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {
                    ActivityCompat.requestPermissions(activity,
                            arrayOf(Manifest.permission.CALL_PHONE), 1)
                } else {
                    ActivityCompat.requestPermissions(activity,
                            arrayOf(Manifest.permission.CALL_PHONE), 1)
                }
            } else {
                var intent = Intent(Intent.ACTION_CALL, Uri.parse(url))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                activity.startActivity(intent)
                val appName = activity.getString(R.string.app_name).replace(": ","_").replace(" ","_")
                var urlstring = FINDASTRO_BASE_URL + "app/login?phone=" + email + "&token=" + token +
                        "&utm_source=FindAstro_app&utm_medium=App_launch&utm_campaign=Android_app&utm_client=" +
                        appName +"&utm_platform=android"
                webview.loadUrl(urlstring)
            }
        }
        if (FindAstroStaticMethods.isNetworkAvailable(mActivity!!)) {
            if (url.equals("mailto:support@findastro.com")) {

            } else {
                super.onPageFinished(view, url)
                urldata = view!!.url.toString()
                //MoEngageEventTracker.setIntoViewActions(mActivity!!, "WebView", urldata)
            }
        } else {
            webview.visibility = View.GONE
            Glide.with(mActivity!!).load(R.drawable.nointernetgif).into(nointernetimg)
            nointerneterrorlayout.visibility = View.VISIBLE
        }
        //MoEngageEventTracker.setIntoViewActions(activity, "WebView", urldata)
    }

    @Override
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
            RESULT_CODE_ICE_CREAM -> {
                var uri: Uri? = null
                uri = data.data
                mUploadMessage!!.onReceiveValue(uri)
                mUploadMessage = null
            }
            REQUEST_CODE_LOLIPOP -> {
                var results: Array<Uri>? = null
                // Check that the response is a good one
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    val dataString = data.dataString
                    if (dataString != null) {
                        results = arrayOf(Uri.parse(dataString))
                    }
                }
                mFilePathCallback!!.onReceiveValue(results)
                mFilePathCallback = null
            }
        }
    }
}