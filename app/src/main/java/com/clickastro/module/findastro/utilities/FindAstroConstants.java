package com.clickastro.module.findastro.utilities;

/**
 * Created by Ajesh-avft on 23-10-2017.
 */

public final class FindAstroConstants {


    public static String BLANK_STRING = "";
   public static String SHARED_PREFERENCE = "ConsultancyApps";


    public static final String STRING_COMMA = ",";
    public static String JAVASCRIPT_IDENTIFIER = "AndroidBridge";
    public static String FINDASTRO_API_URL ="https://api.findastro.com/api/v1/";
    public static String FINDASTRO_BASE_URL = "https://www.findastro.com/";


    //Integer constants
    public static int POSITION_ONE = 1;
    public static int LENGTH_ONE = 1;

}
