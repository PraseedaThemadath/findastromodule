package com.clickastro.module.findastro

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


private var mActivity: AppCompatActivity? = null
private val instance: TermsAndPrivacyFragmentFindAstro? = null
private lateinit var termsprivacytext: TextView
private lateinit var termsprivacytext2: TextView
private lateinit var backbtntermsandprivacy: ImageView
private lateinit var textterms: TextView
private lateinit var textconditions: TextView

class TermsAndPrivacyFragmentFindAstro : Fragment() {
    @Synchronized
    open fun getInstance(action: String?, data: String?): TermsAndPrivacyFragmentFindAstro? {
        return instance
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.terms_and_privacy_findastro_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initvariables(view)

        activity?.onBackPressedDispatcher?.addCallback(mActivity!!, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                isEnabled = false
                activity?.onBackPressed()
            }
        })
        val desired_string = arguments?.getString("termsprivacy")

        if (desired_string.equals("terms")) {
            textterms.setText(R.string.termstext)
            textconditions.setText(R.string.conditions)
            termsprivacytext.text = Html.fromHtml(getString(R.string.termsfindastro))
            termsprivacytext2.text = Html.fromHtml(getString(R.string.termsfindastro2))
        } else {
            textterms.setText(R.string.privacytext)
            textconditions.setText(R.string.policytext)
            termsprivacytext.text = Html.fromHtml(getString(R.string.privacyfindastro))
        }

        backbtntermsandprivacy.setOnClickListener {
            val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
            if (fragmentTransaction != null) {
                fragmentTransaction.replace(R.id.frameLayoutfindastro, FindAstroRegFragment())
                fragmentTransaction.commit()
            }
        }
    }

    private fun initvariables(view: View) {
        termsprivacytext = view.findViewById(R.id.termsprivacytext)
        termsprivacytext2 = view.findViewById(R.id.termsprivacytext2)
        backbtntermsandprivacy = view.findViewById(R.id.backbtntermsandprivacy)
        textterms = view.findViewById(R.id.textterms)
        textconditions = view.findViewById(R.id.conditiontext)
    }

    override fun onSaveInstanceState(outState: Bundle) {}

    override fun onAttach(mContext: Context) {
        super.onAttach(mContext)
        if (mContext is AppCompatActivity) {
            mActivity = mContext
        }
    }
}