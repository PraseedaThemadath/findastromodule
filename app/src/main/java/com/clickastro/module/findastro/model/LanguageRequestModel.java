package com.clickastro.module.findastro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LanguageRequestModel {

@SerializedName("languageIds")
@Expose
private List<String> languageIds = null;

public List<String> getLanguageIds() {
return languageIds;
}

public void setLanguageIds(List<String> languageIds) {
this.languageIds = languageIds;
}

}