package com.clickastro.module.findastro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.clickastro.module.findastro.model.requestotp.OtpModel
import com.clickastro.module.findastro.utilities.FindAstroConstants.BLANK_STRING
import com.clickastro.module.findastro.utilities.FindAstroConstants.FINDASTRO_API_URL
import com.clickastro.module.findastro.utilities.FindastroApiCalls
import com.clickastro.module.findastro.utilities.FindAstroStaticMethods
import com.clickastro.module.findastro.utilities.UserProfile
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private lateinit var mActivity: AppCompatActivity
private val instance: LoginFragmentFindAstro? = null
private var deepLinkAction = BLANK_STRING
private var deepLinkData = BLANK_STRING
private lateinit var signupnowtxt: TextView
private lateinit var continuebtn: Button
private lateinit var backbtnlogin: ImageView
private lateinit var phone: EditText
private lateinit var usercode: String
private lateinit var progressloginfindastro: ProgressBar

class LoginFragmentFindAstro : Fragment() {
    private lateinit var mUser : UserProfile
    companion object {
        @Synchronized
        public fun getInstance(action: String?, data: String?): LoginFragmentFindAstro? {
            deepLinkAction = action!!
            deepLinkData = data!!
            return instance
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login_find_astro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (requireArguments() != null && requireArguments().containsKey("user")){
            mUser = requireArguments().get("user") as UserProfile
        }
        initvariables(view)

        try {
            signupnowtxt.setOnClickListener {
//                MoEngageEventTracker.setButtonClickActions(requireContext(), "SignUp Text", "Login Screen", "SignUp Screen", "NA")
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                if (fragmentTransaction != null) {
                    fragmentTransaction.replace(R.id.frameLayoutfindastro, FindAstroRegFragment())
                    fragmentTransaction.commit()
                }
            }

            continuebtn.setOnClickListener {
                callapi()
            }
//            MoEngageEventTracker.userpagevieweventstrack(requireContext(), "LOGIN_PAGE_VIEWED", "https://api.findastro.com/api/v1/user/otp")
        } catch (e: Exception) {
        }
    }

    public fun callapi() {
        if (FindAstroStaticMethods.isSignedUser()) {
            try {
                if (phone.text.equals("") || phone.text.length < 10) {
                    Toast.makeText(
                        requireContext(),
                        "Please enter valid phone number",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    progressloginfindastro.visibility = View.VISIBLE
                    val retrofit = Retrofit.Builder()
                        .baseUrl(FINDASTRO_API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()

                    val service = retrofit.create(FindastroApiCalls::class.java)
                    val call = service.getrequestsignupotp(phone.text.toString())

                    call.enqueue(object : Callback<OtpModel> {
                        override fun onResponse(
                            call: Call<OtpModel>,
                            response: Response<OtpModel>
                        ) {
                            if ((response.code() == 200 && response.body() != null) || (response.code() == 201 && response.body() != null)) {
                                progressloginfindastro.visibility = View.GONE
//                                MoEngageEventTracker.setButtonClickActions(requireContext(), "Request OTP Login", "Login Screen", "OTP Screen", "NA")
                                usercode = response.body()!!.data.id.toString()

                                val frag = OtpScreenFindastro()
                                val arguments = Bundle()
                                arguments.putString("phone", phone.text.toString())
                                arguments.putString("activity", "login")
                                arguments.putString("id", usercode)
                                arguments.putParcelable("user", mUser)
                                frag.arguments = arguments
                                val fragmentTransaction =
                                    activity?.supportFragmentManager?.beginTransaction()
                                if (fragmentTransaction != null) {
                                    fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                                    fragmentTransaction.commit()
                                }
                            } else {
                                progressloginfindastro.visibility = View.GONE
                                Toast.makeText(
                                    requireContext(),
                                    "" + response.message(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<OtpModel>, t: Throwable) {
                            progressloginfindastro.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "Unable to Generate OTP. Please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    })
                }
            } catch (ex: Exception) {
            }
        } else {
//            signInUser()
        }
    }

    private fun initvariables(view: View) {
        signupnowtxt = view.findViewById(R.id.signupbtn)
        continuebtn = view.findViewById(R.id.btn_continue_login)
        phone = view.findViewById(R.id.phonenumberlogin)
        progressloginfindastro = view.findViewById(R.id.loginprogressfindastro)
    }

    override fun onSaveInstanceState(outState: Bundle) {}
}