package com.clickastro.module.findastro.utilities

import android.os.Handler
import android.os.SystemClock

class CustomTimer(var timeHandler: Handler) : Runnable {
    private var startTime = 0L
    var timeInMilliseconds = 0L
    var timeSwapBuff = 0L
    var updatedTime = 0L
    override fun run() {
        timeInMilliseconds = SystemClock.uptimeMillis() - startTime
        updatedTime = timeSwapBuff + timeInMilliseconds
        timeHandler.postDelayed(this, 0)
    }

    fun startTime() {
        startTime = SystemClock.uptimeMillis()
        timeHandler.postDelayed(this, 0)
    }

    fun stopTime(): String {
        timeSwapBuff += timeInMilliseconds
        var secs = (timeSwapBuff / 1000).toInt()
        val mins = secs / 60
        secs %= 60
        val milliseconds = (updatedTime % 1000).toInt()
        val timeElapsed = (mins.toString() + ":"
                + String.format("%02d", secs) + ":"
                + String.format("%03d", milliseconds))
        //Log.e("Timer>>>", timeElapsed);
        timeHandler.removeCallbacks(this)
        startTime = 0L
        timeInMilliseconds = 0L
        timeSwapBuff = 0L
        updatedTime = 0L
        return timeElapsed
    }
}