package com.clickastro.module.findastro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageAccessResponseModel {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("access_url")
@Expose
private String accessUrl;
@SerializedName("url")
@Expose
private String url;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getAccessUrl() {
return accessUrl;
}

public void setAccessUrl(String accessUrl) {
this.accessUrl = accessUrl;
}

public String getUrl() {
return url;
}

public void setUrl(String url) {
this.url = url;
}

}