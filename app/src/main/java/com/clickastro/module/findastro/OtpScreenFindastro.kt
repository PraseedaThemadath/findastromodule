package com.clickastro.module.findastro

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.clickastro.module.findastro.model.AddUserRequestModel
import com.clickastro.module.findastro.model.AddUserResponseModel
import com.clickastro.module.findastro.model.LanguageRequestModel
import com.clickastro.module.findastro.model.LanguageResponseModel
import com.clickastro.module.findastro.model.RegisterModel.OtpSuccessRegisterModel
import com.clickastro.module.findastro.model.loginModel.OtpSuccessLoginModel
import com.clickastro.module.findastro.model.requestotp.OtpModel
import com.clickastro.module.findastro.utilities.FindAstroConstants.BLANK_STRING
import com.clickastro.module.findastro.utilities.FindAstroConstants.FINDASTRO_API_URL
import com.clickastro.module.findastro.utilities.FindastroApiCalls
import com.clickastro.module.findastro.utilities.SharedPreferenceMethods
import com.clickastro.module.findastro.utilities.UserProfile
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


private var mActivity: AppCompatActivity? = null
private val instance: OtpScreenFindastro? = null
private var deepLinkData = BLANK_STRING
private var deepLinkAction = BLANK_STRING
private lateinit var phonenumbertext: TextView
private lateinit var phonenumtxt: TextView
private lateinit var continuebtn: Button
private lateinit var resend: Button
private var counter = 30
private lateinit var resendbtn: Button
private lateinit var phonenumber: String
private lateinit var usercode: String
private lateinit var fromname: String
private lateinit var otpstring: String
private lateinit var whatsappval: String
private lateinit var backbtnotp: ImageView
private lateinit var dndcheckbox: CheckBox
private lateinit var whatsappcheckbox: CheckBox
private lateinit var termslinlayout: LinearLayout
private lateinit var otpEditText: EditText
private lateinit var progressotpscreen: ProgressBar
var isRegistered = false
var isLangSelected = false
var isuseradded = false
private lateinit var mContext: Context
private var lat: String = ""
private var lng: String = ""
private var country: String = ""
var list: MutableList<String> = mutableListOf("1")
private lateinit var mUser: UserProfile


class OtpScreenFindastro : Fragment() {

    @Synchronized
    open fun getInstance(action: String?, data: String?): OtpScreenFindastro? {
        deepLinkAction = action!!
        deepLinkData = data!!
        return instance
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.otp_screen_findstro_layout, container, false)
//        MoEngageEventTracker.userpagevieweventstrack(requireContext(), "LOGIN_PAGE_VIEWED", "https://api.findastro.com/api/v1/user/otp")

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initvariables(view)
        startTimeCounter()
        mUser = requireArguments().getParcelable("user")!!
        phonenumber = getArguments()?.getString("phone").toString()
        usercode = getArguments()?.getString("id").toString()
        fromname = getArguments()?.getString("activity").toString()

        resend.visibility = View.GONE
        phonenumtxt.visibility = View.VISIBLE

        phonenumtxt.setText("Enter 6 digit OTP sent to " + phonenumber)

        if (fromname.equals("register")) {
            termslinlayout.visibility = View.VISIBLE
        } else {
            termslinlayout.visibility = View.GONE
        }

        resend.setOnClickListener {
            resend.visibility = View.GONE
            phonenumtxt.visibility = View.VISIBLE
            startTimeCounter()

            val retrofit = Retrofit.Builder()
                .baseUrl(FINDASTRO_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val service = retrofit.create(FindastroApiCalls::class.java)
            val call = service.getrequestsignupresendotp(usercode, phonenumber)

            call.enqueue(object : Callback<OtpModel> {
                override fun onResponse(call: Call<OtpModel>, response: Response<OtpModel>) {
                    if (response.code() == 200) {
                        Toast.makeText(
                            requireContext(),
                            "OTP sent to " + phonenumber,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "" + response.message(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<OtpModel>, t: Throwable) {
                    Toast.makeText(
                        requireContext(),
                        "Unable to Generate OTP. Please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }

        continuebtn.setOnClickListener {

            if (otpEditText.text.equals("")) {
                Toast.makeText(requireContext(), "Please enter valid OTP", Toast.LENGTH_SHORT)
                    .show()
            } else {
                otpstring = otpEditText.text.toString()

                if (fromname.equals("register")) {
                    if (!dndcheckbox.isChecked) {
                        Toast.makeText(
                            requireContext(),
                            "Please accept our terms and conditions",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (!whatsappcheckbox.isChecked) {
                        whatsappval = "" + 0
                        registerapicall(progressotpscreen)
                    } else {
                        whatsappval = "" + 1
                        registerapicall(progressotpscreen)
                    }
                } else {
                    loginapicall(progressotpscreen)
                }
            }
        }

        backbtnotp.setOnClickListener {
            if (fromname.equals("register")) {
                val fragmentTransaction = activity?.getSupportFragmentManager()?.beginTransaction()
                if (fragmentTransaction != null) {
                    fragmentTransaction.replace(R.id.frameLayoutfindastro, FindAstroRegFragment())
                    fragmentTransaction.commit()
                }
            } else {
                val fragmentTransaction = activity?.getSupportFragmentManager()?.beginTransaction()
                if (fragmentTransaction != null) {
                    fragmentTransaction.replace(R.id.frameLayoutfindastro, LoginFragmentFindAstro())
                    fragmentTransaction.commit()
                }
            }
        }

    }

    private fun loginapicall(progressDialog: ProgressBar) {
        var age = getAge(mUser.getUserDob())
            if (age > 13) {
                progressDialog.visibility = View.VISIBLE
                val retrofit = Retrofit.Builder()
                    .baseUrl(FINDASTRO_API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                val service = retrofit.create(FindastroApiCalls::class.java)

                val call = service.enterloginotp(usercode, phonenumber, otpstring, "1", "1", "")

                call.enqueue(object : Callback<OtpSuccessLoginModel> {
                    override fun onResponse(
                        call: Call<OtpSuccessLoginModel>,
                        response: Response<OtpSuccessLoginModel>
                    ) {
                        if (response.code() == 200 && response.body()!!.success) {
                            progressDialog.visibility = View.GONE
    //                        MoEngageEventTracker.setTrackingMobileNumber(phonenumber, mContext, true)
                            response.body()!!.userProfile.email
                            if (response.body()!!.userProfile.email == null) {
                                SharedPreferenceMethods.setBoolean(
                                    requireContext(),
                                    "isRegistered",
                                    true
                                )
    //                            MoEngageEventTracker.setButtonClickActions(requireContext(), "Verify OTP", "OTP Screen", "Language Screen", "NA")

                                calllanguageapi(
                                    "Bearer " + response.body()!!.bearer,
                                    response.body()!!.token,
                                    phonenumber,
                                    usercode
                                )
                            } else {

                                val frag = WebViewFindAstro()
                                val arguments = Bundle()
                                arguments.putString(
                                    "email",
                                    response.body()!!.userProfile.phone.toString()
                                )
                                arguments.putString("token", response.body()!!.token)
                                arguments.putParcelable("user", mUser)
                                frag.arguments = arguments
                                val fragmentTransaction =
                                    activity?.getSupportFragmentManager()?.beginTransaction()
                                if (fragmentTransaction != null) {
                                    fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                                    fragmentTransaction.commit()
                                }

                            }

                        } else {
                            progressDialog.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "User is not registered / Invalid OTP. Please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onFailure(call: Call<OtpSuccessLoginModel>, t: Throwable) {
                        progressDialog.visibility = View.GONE
                        Toast.makeText(
                            requireContext(),
                            "Please check your internet connection and try again",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            } else {
                Toast.makeText(requireContext(), "User should be 13 years or older", Toast.LENGTH_SHORT)
                    .show()
            }

    }

    private fun registerapicall(progressDialog: ProgressBar) {
        var age = getAge(mUser.getUserDob())
            if (age > 13) {
                progressDialog.visibility = View.VISIBLE
                val retrofit = Retrofit.Builder()
                    .baseUrl(FINDASTRO_API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                val service = retrofit.create(FindastroApiCalls::class.java)
                val call = service.enterregisterotp(
                    usercode,
                    phonenumber,
                    otpstring,
                    "1",
                    "" + whatsappval,
                    ""
                )

                call.enqueue(object : Callback<OtpSuccessRegisterModel> {
                    override fun onResponse(
                        call: Call<OtpSuccessRegisterModel>,
                        response: Response<OtpSuccessRegisterModel>
                    ) {
                        if (response.code() == 200 && !response.body()!!.isLoginElapse && response.body()!!.success) {
                            progressDialog.visibility = View.GONE
    //                        MoEngageEventTracker.setButtonClickActions(requireContext(), "Verify OTP", "OTP Screen", "Language Screen", "NA")
                            calllanguageapi(
                                "Bearer " + response.body()!!.bearer,
                                response.body()!!.token,
                                phonenumber,
                                usercode
                            )

    //                    StaticMethods.getDefaultUser(requireContext()).userLang
                        } else {
                            progressDialog.visibility = View.GONE
                            if (response.body()!!.success) {

                                val frag = WebViewFindAstro()
                                val arguments = Bundle()
                                arguments.putString(
                                    "email",
                                    response.body()!!.userProfile.phone.toString()
                                )
                                arguments.putString("token", response.body()!!.token)
                                arguments.putParcelable("user", mUser)
                                frag.arguments = arguments
                                val fragmentTransaction =
                                    activity?.getSupportFragmentManager()?.beginTransaction()
                                if (fragmentTransaction != null) {
                                    fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                                    fragmentTransaction.commit()
                                }

                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    "Invalid OTP. Please enter a valid OTP",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<OtpSuccessRegisterModel>, t: Throwable) {
                        progressDialog.visibility = View.GONE
                        Toast.makeText(
                            requireContext(),
                            "OTP is invalid. Please try again",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            } else {
                Toast.makeText(requireContext(), "User should be 13 years or older", Toast.LENGTH_SHORT)
                    .show()
            }

    }

    private fun calllanguageapi(
        bearer: String,
        token: String?,
        phonenumber: String,
        usercode: String
    ) {

        progressotpscreen.visibility = View.VISIBLE

        var selectedlang = mUser.getUserLang()
        if (selectedlang.equals("HIN")) {
            list.add("2")
        } else
            if (selectedlang.equals("TAM")) {
                list.add("6")
            } else
                if (selectedlang.equals("MAL")) {
                    list.add("10")
                } else
                    if (selectedlang.equals("TEL")) {
                        list.add("5")
                    } else
                        if (selectedlang.equals("MAR")) {
                            list.add("4")
                        } else
                            if (selectedlang.equals("KAN")) {
                                list.add("8")
                            } else
                                if (selectedlang.equals("ORI")) {
                                    list.add("9")
                                } else
                                    if (selectedlang.equals("GUJ")) {
                                        list.add("7")
                                    }

        var langlist: LanguageRequestModel = LanguageRequestModel()
        langlist.languageIds = list
        val retrofit = Retrofit.Builder()
            .baseUrl(FINDASTRO_API_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(FindastroApiCalls::class.java)
        val call = service.languageSelectioncall(bearer, token!!, langlist)

        call.enqueue(object : Callback<LanguageResponseModel> {
            override fun onResponse(
                call: Call<LanguageResponseModel>,
                response: Response<LanguageResponseModel>
            ) {
                if (response.code() == 200 && response.body()!!.success) {
                    progressotpscreen.visibility = View.GONE
//                    MoEngageEventTracker.languagetrack(requireContext(), "LANGUAGE_PAGE_SUBMIT", "https://api.findastro.com/api/v1/profile/user/language", list.size.toString())
                    SharedPreferenceMethods.setBoolean(requireContext(), "languageSelected", true)
//                    MoEngageEventTracker.setButtonClickActions(requireContext(), "Select Language", "Language Activity", "User Add", "NA")
                    useraddapicall(phonenumber, bearer, token, usercode)

                } else {
                    progressotpscreen.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        "Unable to update languages, please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<LanguageResponseModel>, t: Throwable) {
                progressotpscreen.visibility = View.GONE
                Toast.makeText(
                    requireContext(),
                    "Please check your internet connection and try again",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun useraddapicall(
        phonenumber: String,
        bearer: String,
        token: String,
        usercode: String
    ) {
        progressotpscreen.visibility = View.VISIBLE
        var addUserRequestModel: AddUserRequestModel = AddUserRequestModel()
        addUserRequestModel.name = mUser.getUserName()
        addUserRequestModel.dateOfBirth = mUser.getUserDob()?.replace("-", "/")
        //addUserRequestModel.email = mUser.(requireContext())
        addUserRequestModel.gender = mUser.getUserGender()
        if(mUser.getHoroscopeStyle().equals("0")){
            addUserRequestModel.horoscopeStyle = "South Indian"
        }else if(mUser.getHoroscopeStyle().equals("1")){
            addUserRequestModel.horoscopeStyle = "North Indian"
        }else if(mUser.getHoroscopeStyle().equals("2")){
            addUserRequestModel.horoscopeStyle = "North Indian"
        }else if(mUser.getHoroscopeStyle().equals("3")){
            addUserRequestModel.horoscopeStyle = "South Indian"
        }
//        addUserRequestModel.horoscopeStyle = StaticMethods.getDefaultUser(requireContext()).horoscopeStyle
        addUserRequestModel.phone = phonenumber
        addUserRequestModel.placeOfBirth =mUser.getUserPob()
        val format1: DateFormat = SimpleDateFormat("hh:mm:ss")
        try {
            val date: Date = format1.parse(mUser.getUserTob().toString())
            val format2 = SimpleDateFormat("hh:mm a")
            addUserRequestModel.timeOfBirth = format2.format(date).toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        addUserRequestModel.profileUrl = ""
        try {
            var userdetails = mUser.getUserPlaceJsonObject()
            var jsonobj = userdetails
            lat = jsonobj?.getString("latitude_deg") ?:""
            lng = jsonobj?.getString("longitude_deg") ?: ""
            var place = jsonobj?.getString("place_name")
            var parts: List<String> = place!!.split(",")
            country = parts[2]
            addUserRequestModel.country = country
            addUserRequestModel.lat = lat
            addUserRequestModel.lon = lng
        } catch (e: Exception) {

        }

        val retrofit = Retrofit.Builder()
            .baseUrl(FINDASTRO_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(FindastroApiCalls::class.java)
        val call = service.adduserapicall(bearer, token, addUserRequestModel)

        call.enqueue(object : Callback<AddUserResponseModel> {
            override fun onResponse(
                call: Call<AddUserResponseModel>,
                response: Response<AddUserResponseModel>
            ) {
                if (response.code() == 200 && response.body()!!.success) {
                    progressotpscreen.visibility = View.GONE

                    var date = Date()
                    val format = SimpleDateFormat("dd/MM/yyyy")
                    try {
                        date = format.parse(addUserRequestModel.dateOfBirth)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                    SharedPreferenceMethods.setBoolean(requireContext(), "loadwebview", true)
                    SharedPreferenceMethods.setToSharedPreference(
                        requireContext(),
                        "email",
                        addUserRequestModel.phone
                    )
                    SharedPreferenceMethods.setToSharedPreference(requireContext(), "token", token)
                    SharedPreferenceMethods.setBoolean(requireContext(), "useradded", true)
//                    MoEngageEventTracker.setButtonClickActions(requireContext(), "Add User Button", "Add User Activity", "WebView Screen", "NA")


                    val frag = WebViewFindAstro()
                    val arguments = Bundle()
                    arguments.putString("email", addUserRequestModel.phone.toString())
                    arguments.putString("token", token)
                    arguments.putParcelable("user", mUser)
                    frag.arguments = arguments
                    val fragmentTransaction =
                        activity?.getSupportFragmentManager()?.beginTransaction()
                    if (fragmentTransaction != null) {
                        fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                        fragmentTransaction.commit()
                    }
                } else {
                    progressotpscreen.visibility = View.GONE

                    SharedPreferenceMethods.setBoolean(requireContext(), "loadwebview", true)
                    SharedPreferenceMethods.setToSharedPreference(
                        requireContext(),
                        "email",
                        addUserRequestModel.phone
                    )
                    SharedPreferenceMethods.setToSharedPreference(requireContext(), "token", token)
                    SharedPreferenceMethods.setBoolean(requireContext(), "useradded", true)

                    var date = Date()
                    val format = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                    try {
                        date = format.parse(addUserRequestModel.dateOfBirth)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

//                    MoEngageEventTracker.setButtonClickActions(requireContext(), "Add User Button", "Add User Activity", "WebView Screen", "NA")

                    val frag = WebViewFindAstro()
                    val arguments = Bundle()
                    arguments.putString("email", addUserRequestModel.phone.toString())
                    arguments.putString("token", token)
                    arguments.putParcelable("user", mUser)
                    frag.arguments = arguments
                    val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                    if (fragmentTransaction != null) {
                        fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                        fragmentTransaction.commit()
                    }
//                    MoEngageEventTracker.userpagevieweventstrack(requireContext(), "USERPROFILE_CREATED", "https://api.findastro.com/api/v1/profile/user/edit")
                }
            }

            override fun onFailure(call: Call<AddUserResponseModel>, t: Throwable) {
                progressotpscreen.visibility = View.GONE
                Toast.makeText(
                    requireContext(),
                    "Please check your internet connection and try again",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    private fun initvariables(view: View) {
        phonenumbertext = view.findViewById(R.id.textviewotpphonennumber)
        continuebtn = view.findViewById(R.id.btn_verify)
        resend = view.findViewById(R.id.btnresendotp)
        resendbtn = view.findViewById(R.id.btnresendotp)
        phonenumtxt = view.findViewById(R.id.phonenumotp)
        backbtnotp = view.findViewById(R.id.back_btn_otp)
        dndcheckbox = view.findViewById(R.id.dndtermscheckbox)
        whatsappcheckbox = view.findViewById(R.id.whatsappcheckbox)
        termslinlayout = view.findViewById(R.id.termslinlayout)
        otpEditText = view.findViewById(R.id.otpedittextfindastro)
        progressotpscreen = view.findViewById(R.id.progressotpscreen)
    }

    fun startTimeCounter() {
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                phonenumbertext.setText("Request OTP in " + counter.toString() + "s")
                counter--
            }

            override fun onFinish() {
                phonenumbertext.visibility = View.GONE
                resend.visibility = View.VISIBLE
                counter = 30
            }
        }.start()
    }

    fun getAge(date: String?): Int {
        var age = 0
        try {
            val df = SimpleDateFormat("dd-mm-yyyy")
            val date1: Date = df.parse(date)
            val now = Calendar.getInstance()
            val dob = Calendar.getInstance()
            dob.time = date1
            require(!dob.after(now)) { "Can't be born in the future" }
            val year1 = now[Calendar.YEAR]
            val year2 = dob[Calendar.YEAR]
            age = year1 - year2
            val month1 = now[Calendar.MONTH]
            val month2 = dob[Calendar.MONTH]
            if (month2 > month1) {
                age--
            } else if (month1 == month2) {
                val day1 = now[Calendar.DAY_OF_MONTH]
                val day2 = dob[Calendar.DAY_OF_MONTH]
                if (day2 > day1) {
                    age--
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return age
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onSaveInstanceState(outState: Bundle) {}
}