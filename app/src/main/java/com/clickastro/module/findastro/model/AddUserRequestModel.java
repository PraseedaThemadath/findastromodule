package com.clickastro.module.findastro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddUserRequestModel {

@SerializedName("name")
@Expose
private String name;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("email")
@Expose
private String email;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("horoscopeStyle")
@Expose
private String horoscopeStyle;
@SerializedName("dateOfBirth")
@Expose
private String dateOfBirth;
@SerializedName("placeOfBirth")
@Expose
private String placeOfBirth;
@SerializedName("timeOfBirth")
@Expose
private String timeOfBirth;
@SerializedName("profileUrl")
@Expose
private String profileUrl;
@SerializedName("lat")
@Expose
private String lat;
@SerializedName("lon")
@Expose
private String lon;
@SerializedName("country")
@Expose
private String country;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getHoroscopeStyle() {
return horoscopeStyle;
}

public void setHoroscopeStyle(String horoscopeStyle) {
this.horoscopeStyle = horoscopeStyle;
}

public String getDateOfBirth() {
return dateOfBirth;
}

public void setDateOfBirth(String dateOfBirth) {
this.dateOfBirth = dateOfBirth;
}

public String getPlaceOfBirth() {
return placeOfBirth;
}

public void setPlaceOfBirth(String placeOfBirth) {
this.placeOfBirth = placeOfBirth;
}

public String getTimeOfBirth() {
return timeOfBirth;
}

public void setTimeOfBirth(String timeOfBirth) {
this.timeOfBirth = timeOfBirth;
}

public String getProfileUrl() {
return profileUrl;
}

public void setProfileUrl(String profileUrl) {
this.profileUrl = profileUrl;
}

public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}