package com.clickastro.module.findastro

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.clickastro.module.findastro.model.requestotp.OtpModel
import com.clickastro.module.findastro.utilities.*
import com.clickastro.module.findastro.utilities.FindAstroConstants.BLANK_STRING
import com.clickastro.module.findastro.utilities.FindAstroConstants.FINDASTRO_API_URL
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.ClassCastException

//private var mContext =
private var mActivity = AppCompatActivity()
private var deepLinkData = BLANK_STRING
private var deepLinkAction = BLANK_STRING
private lateinit var loginnowtxt: TextView
private lateinit var continuebtn: Button
private lateinit var backbtnsignup: ImageView
private lateinit var phone: EditText
private lateinit var usercode: String
private lateinit var countrycode: String
private lateinit var termsncondition: TextView
private lateinit var checkbox: CheckBox
private lateinit var regprogress: ProgressBar
private var instance: FindAstroRegFragment? = null
private lateinit var mUserAuthenticate: IUserAuthenticate



//private lateinit var mContext: Context
private var btnclick: String = ""


class FindAstroRegFragment : Fragment() {
private lateinit var mUser : UserProfile
private var mUserSignIn: IUserAuthenticate? = null

    companion object {
        fun getInstance(action: String?, data: String?): FindAstroRegFragment? = FindAstroRegFragment().apply {
            //arguments = bundleOf(deepLinkData to data, deepLinkAction to action)
            return instance
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.findastro_reg_fragment, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initvariables(view)
        if (requireArguments() != null && requireArguments().containsKey("user")){
            mUser = requireArguments().get("user") as UserProfile
        }
        val loadwebview = SharedPreferenceMethods.getBoolean(activity, "loadwebview")
        val email = SharedPreferenceMethods.getFromSharedPreference(activity, "email")
        val token = SharedPreferenceMethods.getFromSharedPreference(activity, "token")
        try {
            if (loadwebview) {
                val frag = WebViewFindAstro()
                val arguments = Bundle()
                arguments.putString("email", email)
                arguments.putString("token", token)
                frag.arguments = arguments
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                if (fragmentTransaction != null) {
                    fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                    fragmentTransaction.commit()
                }
            }
            loginnowtxt.setOnClickListener {
                btnclick = "loginnow"
                if (FindAstroStaticMethods.isSignedUser()) {
                    loginfragmentattach()
                } else {
                    mUserAuthenticate.initiateUserAuthentication()
                }
            }

            continuebtn.setOnClickListener {
                btnclick = "continue"
                callapi()
            }

            val text = "I accept the terms and conditions and privacy policy"
            val ss = SpannableString(text)
            val clickableSpan1: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val frag = TermsAndPrivacyFragmentFindAstro()
                    val arguments = Bundle()
                    arguments.putString("termsprivacy", "terms")
                    frag.arguments = arguments
                    val fragmentTransaction = activity?.getSupportFragmentManager()?.beginTransaction()
                    if (fragmentTransaction != null) {
                        fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                        fragmentTransaction.commit()
                    }
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Color.BLUE
                    ds.isUnderlineText = false
                }
            }
            val clickableSpan2: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val frag = TermsAndPrivacyFragmentFindAstro()
                    val arguments = Bundle()
                    arguments.putString("termsprivacy", "privacy")
                    frag.arguments = arguments
                    val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                    if (fragmentTransaction != null) {
                        fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                        fragmentTransaction.commit()
                    }
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = Color.BLUE
                    ds.isUnderlineText = false
                }
            }
            ss.setSpan(clickableSpan1, 13, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            ss.setSpan(clickableSpan2, 38, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            termsncondition.text = ss
            termsncondition.movementMethod = LinkMovementMethod.getInstance()
        } catch (e: Exception) {
        }
    }

    private fun loginfragmentattach() {
        val fragmentTransaction = mActivity?.supportFragmentManager?.beginTransaction()
        val arguments = Bundle()
        arguments.putParcelable("user", mUser)
        val loginFragmentFindAstro = LoginFragmentFindAstro()
        loginFragmentFindAstro.arguments = arguments
        if (fragmentTransaction != null) {
            fragmentTransaction.replace(R.id.frameLayoutfindastro, loginFragmentFindAstro)
            fragmentTransaction.commit()
        }
    }


    fun onSignInSuccess(profile: UserProfile) {
        mUser = profile
        if (btnclick.equals("loginnow")) {
            loginfragmentattach()
        } else {
            callapi()
        }
    }

     fun callapi() {
        if (FindAstroStaticMethods.isSignedUser()) {
            try {
                if (phone.text.equals("") || phone.text.length < 10) {
                    Toast.makeText(requireContext(), "Please enter valid phone number", Toast.LENGTH_SHORT).show()
                } else if (!checkbox.isChecked) {
                    Toast.makeText(requireContext(), "Please accept terms and conditions to continue", Toast.LENGTH_SHORT).show()
                } else {
                    regprogress.visibility = View.VISIBLE
                    val retrofit = Retrofit.Builder()
                        .baseUrl(FINDASTRO_API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()

                    val service = retrofit.create(FindastroApiCalls::class.java)
                    val call = service.getrequestsignupotp(phone.text.toString())

                    call.enqueue(object : Callback<OtpModel> {
                        override fun onResponse(call: Call<OtpModel>, response: Response<OtpModel>) {
                            regprogress.visibility = View.GONE
                            try {
                                if ((response.code() == 200 && response.body()!!.data != null) || (response.code() == 201 && response.body()!!.data != null)) {
                                    //setTrackingMobileNumber(phone.text.toString(), requireContext(), true)
                                    //MoEngageEventTracker.setButtonClickActionsfindastro(requireContext(), "Request OTP Registration", "Registration Screen", "OTP Screen", "NA")
                                    usercode = response.body()!!.data.id.toString()
                                    val frag = OtpScreenFindastro()
                                    val arguments = Bundle()
                                    arguments.putString("phone", phone.text.toString())
                                    arguments.putString("activity", "register")
                                    arguments.putString("id", usercode)
                                    arguments.putParcelable("user", mUser)
                                    frag.arguments = arguments
                                    val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                                    if (fragmentTransaction != null) {
                                        fragmentTransaction.replace(R.id.frameLayoutfindastro, frag)
                                        fragmentTransaction.commit()
                                    }
                                } else {
                                    //setTrackingMobileNumber(phone.text.toString(), requireContext(), false)
                                    regprogress.visibility = View.GONE
                                    Toast.makeText(activity, "Please check your internet connection and try again", Toast.LENGTH_SHORT).show()
                                }
                            } catch (e: Exception) {
                            }
                        }

                        override fun onFailure(call: Call<OtpModel>, t: Throwable) {
                            regprogress.visibility = View.GONE
                            //setTrackingMobileNumber(phone.text.toString(), requireContext(), false)
                            Toast.makeText(activity, "Unable to Generate OTP. Please try again", Toast.LENGTH_SHORT).show()
                        }
                    })
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        } else {
            mUserAuthenticate.initiateUserAuthentication()
        }
    }

    private fun initvariables(view: View?) {
        if (view != null) {
            loginnowtxt = view.findViewById(R.id.loginnowtxt)
            continuebtn = view.findViewById(R.id.btn_continue_signup)
            phone = view.findViewById(R.id.phonenumbersignup)
            termsncondition = view.findViewById(R.id.termsandcondition)
            checkbox = view.findViewById(R.id.checkboxterms)
            regprogress = view.findViewById(R.id.regprogress)
        }
    }


    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (activity is IUserAuthenticate) {
            mUserAuthenticate = activity
        } else {
            throw ClassCastException("Must implement interface IUserAuthenticate")
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onAttach(mContext: Context) {
        super.onAttach(mContext)
        if (mContext is AppCompatActivity) {
            mActivity = mContext
        }
        instance = this
    }
}