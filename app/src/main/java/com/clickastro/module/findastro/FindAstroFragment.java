package com.clickastro.module.findastro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.clickastro.module.findastro.utilities.IUserAuthenticate;

import org.jetbrains.annotations.NotNull;

import static com.clickastro.module.findastro.utilities.FindAstroConstants.BLANK_STRING;


public class FindAstroFragment extends Fragment {
    private static FindAstroFragment instance;
    public static String deepLinkAction = BLANK_STRING;
    public static String deepLinkData = BLANK_STRING;
    private static IUserAuthenticate mSignInInterface;



    public static synchronized FindAstroFragment getInstance(String action, String data, Bundle arguments) {
        deepLinkAction = action;
        deepLinkData = data;
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.findastro_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            FindAstroRegFragment regFragment = new FindAstroRegFragment();
            regFragment.setArguments(requireArguments());
            fragmentTransaction.replace(R.id.frameLayoutfindastro, regFragment);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
    }
}
