package com.clickastro.module.findastro.model.loginModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpSuccessLoginModel {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("bearer")
@Expose
private String bearer;
@SerializedName("token")
@Expose
private String token;
@SerializedName("userProfile")
@Expose
private UserProfile userProfile;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getBearer() {
return bearer;
}

public void setBearer(String bearer) {
this.bearer = bearer;
}

public String getToken() {
return token;
}

public void setToken(String token) {
this.token = token;
}

public UserProfile getUserProfile() {
return userProfile;
}

public void setUserProfile(UserProfile userProfile) {
this.userProfile = userProfile;
}

}