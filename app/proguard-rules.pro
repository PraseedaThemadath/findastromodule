# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepattributes Signature -keep class com.clickastro.module.findastro.model.LanguageRequestModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.LanguageRequestModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.ImageAccessResponseModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.FailResponseModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.AddUserResponseModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.AddUserRequestModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.requestotp.Data { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.requestotp.OtpModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.loginModel.UserProfile { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.RegisterModel.UserProfile { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.loginModel.OtpSuccessLoginModel { *; }
-keepattributes Signature -keep class com.clickastro.module.findastro.model.RegisterModel.OtpSuccessRegisterModel { *; }
